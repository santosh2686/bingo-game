const express = require('express');
const path = require('path');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http, {path: '/api'});
const port = process.env.PORT || 5050;

const Player = require('./utilities/Player');
const Game = require('./utilities/Game');

 app.use(express.static(__dirname + '/'));
 app.get('/', function (req, res) {
 res.sendFile(path.join(__dirname + '/index.html'));
 });


const game = new Game();

io.on('connection', function (socket) {
  console.log('Connected =>', socket.id);

  socket.on('disconnect', function () {
    console.log('Disconnected =>', socket.id);
    game.removePlayer(socket.id);
    io.emit('playerList', game.getPlayerList());
  });

  socket.on('gameStart', function () {
    game.startGame();
    io.emit('gameOn', true);
  });

  socket.on('gameStop', function () {
    game.stopGame();
    io.emit('gameOn', false);
  });

  socket.on('getInitialData', function () {
    socket.emit('initialItems', {
      drawnNumbers: game.getDrawnNumbers(),
      isGameOn: game.isGameActive(),
      playerList: game.getPlayerList()
    });
  });

  socket.on('getPlayerInitialData', function (name) {
    const player = new Player(socket.id, name, game.issueCard());
    game.addPlayer(player);
    socket.emit('playerInitialData', player);
    io.emit('playerList', game.getPlayerList());
  });

  socket.on('drawNewNumber', function () {
    game.drawNewNumber();
    io.emit('numberDrawn', game.lastDrawnNumber());
  });


  socket.on('restartGame', function () {
    io.emit('userMessage', '');
    game.reset();
    const playerList = game.getPlayerList();
    socket.emit('initialItems', {
      drawnNumbers: game.getDrawnNumbers(),
      isGameOn: game.isGameActive(),
      playerList: playerList
    });
    playerList.forEach(function (player) {
      socket.broadcast.to(player.id).emit('newCard', {
        card: game.issueCard(),
        drawnNumbers: game.getDrawnNumbers()
      });
    });
  });

  socket.on('checkMyBingo', function (data) {
    io.emit('userMessage', `User ${ data.name } has requested for Bingo`);
    const isBingo = game.checkBingo(data.numInCard);
    if (isBingo) {
      game.setWonPlayers(data.name);
      const listOfWonPlayers = game.getWonPlayers().join(',');
      io.emit('userMessage', `User ${ listOfWonPlayers } has won the Game`);
    } else {
      socket.broadcast.to(data.id).emit('noBingo', 'This is not a valid bingo');
      io.emit('userMessage', "Not valid Bingo, Game Continue!!");
    }
  });

  socket.on('removePlayer', function () {
    game.removePlayer(socket.id);
  });

});

http.listen(port, function () {
  console.log('listening on *:' + port);
});
