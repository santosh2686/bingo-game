function Game() {
  this.selectedNumbers = [];
  this.players = [];
  this.isGameOn = false;
  this.currentNumber = 0;
  this.wonPlayers = [];
}

Game.prototype.startGame = function () {
  this.isGameOn = true;
};

Game.prototype.stopGame = function () {
  this.isGameOn = false;
};

Game.prototype.isGameActive = function () {
  return this.isGameOn;
};

Game.prototype.issueCard = function () {
  let result = [];
  while(result.length<25) {
    const num = Math.floor((Math.random() * 99) + 1);
    if(result.indexOf(num) === -1) {
      result.push(num);
    }
  }
  return result.sort(function (a, b) {
    return a - b;
  });
};

Game.prototype.getNewNumber = function () {
  const num = Math.floor((Math.random() * 99) + 1);
  if(this.selectedNumbers.indexOf(num) === -1) {
    return num;
  } else {
    return this.getNewNumber();
  }
};

Game.prototype.drawNewNumber = function() {
  this.currentNumber = this.getNewNumber();
  this.selectedNumbers.push(this.currentNumber);
};

Game.prototype.lastDrawnNumber = function () {
  return this.currentNumber;
};

Game.prototype.getDrawnNumbers = function () {
  return this.selectedNumbers;
};

Game.prototype.reset = function () {
  this.selectedNumbers = [];
};

Game.prototype.checkBingo = function(playerNum) {
  let isPresent = true;
  playerNum.forEach((num) => {
    if(this.selectedNumbers.indexOf(num) === -1) {
      isPresent = false;
    }
  });
  return isPresent;
};

Game.prototype.addPlayer = function (player) {
  this.players.push(player);
};

Game.prototype.removePlayer = function (playerId) {

  let playerIndex = -1;
  for(let i = 0; i < this.players.length; i++){
    if(this.players[i].id === playerId) {
      playerIndex = i;
      break;
    }
  }
  if (playerIndex !== -1) {
    this.players.splice(playerIndex, 1);
  }
};

Game.prototype.getPlayerList = function () {
  return this.players;
};


Game.prototype.setWonPlayers = function () {
  this.wonPlayers.push(name);
};

Game.prototype.getWonPlayers = function () {
  return this.wonPlayers;
};

module.exports = Game;