function Player(id, name, card) {
  this.id = id;
  this.name = name;
  this.status = 'online';
  this.card = card;
  this.isActive = true
}

module.exports = Player;

