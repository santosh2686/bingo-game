export const mapDispatchToProps = (dispatch) => {
  return {}
};

export const mapStateToProps = (state) => {
  const { isGameOn, drawnNumbers, playerList } = state;
  return {
    isGameOn,
    drawnNumbers,
    playerList
  }
};
