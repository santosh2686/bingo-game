import React from 'react';
import PropTypes from 'prop-types';

import {connect} from 'react-redux';
import {HashRouter as Router, Route, Link} from "react-router-dom";
import {onComponentDidMount} from 'react-redux-lifecycle';
import {mapStateToProps, mapDispatchToProps} from './container.conf';
import {loadInitialDataSocket} from '../actions';

import GameMaster from '../components/GameMaster/GameMaster.jsx';
import Player from '../components/Player/Player.jsx';
import UserList from '../components/UserList/UserList.jsx';

const Container = ({
  drawnNumbers,
  playerList,
  isGameOn
}) => {
  let numberList;
  if(drawnNumbers.length > 0) {
    numberList = drawnNumbers.map((number) =>
      <div key={number}>{number}</div>
    );
  } else {
    numberList = <span>No. Number drawn.</span>
  }

  return (
    <Router>
      <div className="container">
        <div className="header">
          <div className="inner-container">A Bingo Game</div>
        </div>

        <div className="page-data inner-container">
          <div className="game">
            <div className="drawn-numbers">
              {numberList}
            </div>
              <Route exact path="/" component={Player}/>
              <Route path="/master" component={GameMaster}/>
          </div>
          <UserList list={playerList}/>
        </div>
      </div>
    </Router>
  );
};

Container.propTypes = {
  drawnNumbers: PropTypes.array,
  playerList: PropTypes.array,
  isGameOn: PropTypes.bool,
};

Container.defaultProps = {
  drawnNumbers: [],
  playerList: [],
  isGameOn: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(onComponentDidMount(loadInitialDataSocket)(Container));
