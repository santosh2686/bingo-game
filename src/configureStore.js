import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './reducers';


export const configureStore = (initialState) => {
  const enhancer = compose(
    applyMiddleware(
      thunkMiddleware
    )
  );
  return createStore(rootReducer, initialState, enhancer);
};
