import {
  INITIAL_DATA,
  GAME_START_STOP,
  PLAYER_INITIAL_DATA,
  DRAW_NEW_NUMBER,
  MARK_NUMBER_ON_CARD,
  SHOW_MESSAGE_TO_PLAYER,
  UPDATE_PLAYER_LIST,
  RESTART_GAME
} from '../actions';

const reducer = (state, action) => {
  switch (action.type) {
    case INITIAL_DATA: {
      return {
        ...state,
        drawnNumbers: action.items.drawnNumbers,
        isGameOn: action.items.isGameOn,
        playerList: action.items.playerList
      };
    }
    case PLAYER_INITIAL_DATA: {
      return {
        ...state,
        cardDetail: action.items.card,
        id: action.items.id,
        name: action.items.name,
        isActive: action.items.isActive
      };
    }
    case UPDATE_PLAYER_LIST: {
      return {
        ...state,
        playerList: action.res
      }
    }
    case GAME_START_STOP: {
      return {
        ...state,
        isGameOn: action.flag
      };
    }
    case DRAW_NEW_NUMBER: {
      return {
        ...state,
        drawnNumbers: [...state.drawnNumbers, action.num],
      }
    }
    case MARK_NUMBER_ON_CARD: {
      return {
        ...state,
        numInCard: [...state.numInCard, action.num],
        playerMsg: ''
      }
    }
    case SHOW_MESSAGE_TO_PLAYER: {
      return {
        ...state,
        playerMsg: action.msg,
      }
    }
    case RESTART_GAME: {
      return {
        ...state,
        drawnNumbers: action.res.drawnNumbers,
        cardDetail: action.res.card
      }
    }
    default:
      return state;
  }
};

export default reducer;
