export const INITIAL_DATA = 'INITIAL_DATA';
export const GAME_START_STOP = 'GAME_START_STOP';
export const PLAYER_INITIAL_DATA = 'PLAYER_INITIAL_DATA';
export const DRAW_NEW_NUMBER = 'DRAW_NEW_NUMBER';
export const CHECK_BINGO = 'CHECK_BINGO';
export const MARK_NUMBER_ON_CARD = 'MARK_NUMBER_ON_CARD';
export const SHOW_MESSAGE_TO_PLAYER = 'SHOW_MESSAGE_TO_PLAYER';
export const UPDATE_PLAYER_LIST = 'UPDATE_PLAYER_LIST';
export const RESTART_GAME = 'RESTART_GAME';

export const initialData = (res) => {
  return {
    type: INITIAL_DATA,
    items: res
  }
};

export const playerInitialData = (res) => {
  return {
    type: PLAYER_INITIAL_DATA,
    items: res
  }
};

export const gameStartStop = (flag) => {
  return {
    type: GAME_START_STOP,
    flag
  }
};

export const updateDrawnNumber = (num) => {
  return {
    type: DRAW_NEW_NUMBER,
    num
  }
};

export const markNumberOnCard = (num) => {
  return {
    type: MARK_NUMBER_ON_CARD,
    num
  }
};

export const showMessageToPlayer = (msg) => {
  return {
    type: SHOW_MESSAGE_TO_PLAYER,
    msg
  }
};

export const updatePlayerList = (res) => {
  return {
    type: UPDATE_PLAYER_LIST,
    res
  }
};

export const restartGame = (res) => {
  return {
    type: RESTART_GAME,
    res
  }
};

export const checkValidNumber = (num) => {
  return (dispatch, getState) => {
    const {drawnNumbers} = getState();
    if (drawnNumbers.indexOf(num) > -1) {
      dispatch(markNumberOnCard(num));
    } else {
      dispatch(showMessageToPlayer('You can not select this number.'));
    }
  }
};

export const loadInitialDataSocket = () => {
  return (dispatch) => {
    socket.emit('getInitialData');
    socket.on('initialItems', (res) => {
      dispatch(initialData(res))
    });
    socket.on('gameOn', (flag) => {
      dispatch(gameStartStop(flag))
    });
    socket.on('numberDrawn', (num) => {
      dispatch(updateDrawnNumber(num))
    });
    socket.on('playerList', (res) => {
      dispatch(updatePlayerList(res))
    });
    socket.on('userMessage', (msg) => {
      dispatch(showMessageToPlayer(msg));
    });
    socket.on('noBingo', (msg) => {
      dispatch(showMessageToPlayer(msg));
    });
    socket.on('newCard', (res) => {
      dispatch(restartGame(res));
    });
  }
};

export const loadPlayerInitialData = (name) => {
  return (dispatch) => {
    socket.emit('getPlayerInitialData', name);
    socket.on('playerInitialData', (res) => {
      dispatch(playerInitialData(res))
    });
  }
};

export const startGame = () => {
  return (dispatch) => {
    socket.emit('gameStart');
  }
};

export const stopGame = () => {
  return (dispatch) => {
    socket.emit('gameStop');
  }
};

export const resetGame = () => {
  return (dispatch) => {
    socket.emit('restartGame');
  }
};

export const drawNewNumber = () => {
  return (dispatch) => {
    socket.emit('drawNewNumber');
  }
};

export const checkMyBingo = () => {
  return (dispatch, getState) => {
    const {id, numInCard, name} = getState();
    if (numInCard.length === 25) {
      socket.emit('checkMyBingo', {id, numInCard, name});
    } else {
      dispatch(showMessageToPlayer('Not a valid bingo.'));
    }
  }
};


