import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {mapStateToProps, mapDispatchToProps} from './cardCell.conf';

const CardCell = ({
  numInCard,
  number,
  numberClicked
}) => {
  if(numInCard.indexOf(number) >= 0) {
    return (<div className="active">{number}</div>);
  } else {
    return (<div onClick={numberClicked}>{number}</div>);
  }
};

CardCell.propTypes = {
  numInCard: PropTypes.array,
  number: PropTypes.number,
  numberClicked: PropTypes.func,
};
CardCell.defaultProps = {
  numInCard: []
};

export default connect(mapStateToProps, mapDispatchToProps)(CardCell);
