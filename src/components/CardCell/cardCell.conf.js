import { checkValidNumber } from '../../actions';

export const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    numberClicked: () => {
      dispatch(checkValidNumber(ownProps.number));
    }
  }
};

export const mapStateToProps = (state) => {
  const { numInCard } = state;
  return {
    numInCard
  };
};
