import React from 'react';
import PropTypes from 'prop-types';
import CardCell from '../CardCell/CardCell.jsx';

const BingoCard = ({
  card,
}) => {
  const bingoCard = card.map((number) => {
    return <CardCell key={number} number={number} />;
  });
  return (
    <div className="card">
      {bingoCard}
    </div>
  )
};

BingoCard.prototypes = {
  card: PropTypes.array
};

BingoCard.defaultProps = {
  card: []
};

export default BingoCard;
