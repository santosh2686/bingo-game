import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {mapStateToProps, mapDispatchToProps} from './player.conf';

import BingoCard from '../BingoCard/BingoCard.jsx';
import UserForm from '../UserForm/UserForm.jsx';
const Player = ({
  cardDetail,
  id,
  numInCard,
  checkBingo,
  playerMsg,
  isActive,
  isGameOn
}) => {
  if(!isActive) {
    return (<UserForm/>);
  }

  if(!isGameOn) {
    return (<div className="game-start-msg">Wait for game master to start the game.</div>);
  }

  if(isActive) {
    return (
      <div className="player">
        <div>
          <div className="user-message">{playerMsg}</div>
          <BingoCard numCard={numInCard} card={cardDetail} userId={id}/>
        </div>
        <div className="bingo-button">
          <button onClick={checkBingo}>BINGO</button>
        </div>
      </div>
    )
  }
};

Player.prototypes = {
  cardDetail: PropTypes.array,
  id: PropTypes.string,
  numInCard: PropTypes.array,
  checkBingo: PropTypes.func,
  playerMsg: PropTypes.string,
  isActive: PropTypes.bool,
  isGameOn: PropTypes.bool,
};

Player.defaultProps = {
  cardDetail: [],
  id: '',
  numInCard: [],
  playerMsg: '',
  isActive: false,
  isGameOn: false
};

export default connect(mapStateToProps, mapDispatchToProps)(Player);
