import { checkMyBingo } from '../../actions';

export const mapDispatchToProps = (dispatch) => {
  return {
    checkBingo: () => {
      dispatch(checkMyBingo());
    }
  }
};

export const mapStateToProps = (state) => {
  const { numInCard, isGameOn, cardDetail, id, playerMsg, isActive } = state;
  return {
    cardDetail,
    id,
    numInCard,
    playerMsg,
    isActive,
    isGameOn
  }
};
