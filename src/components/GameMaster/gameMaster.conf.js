import { drawNewNumber, startGame, stopGame, resetGame } from '../../actions';

export const mapDispatchToProps = (dispatch) => {
  return {
    drawNewNumber: () => {
      dispatch(drawNewNumber());
    },
    startGame: () => {
      dispatch(startGame());
    },
    stopGame: () => {
      dispatch(stopGame());
    },
    restartGame: () => {
      dispatch(resetGame());
    }
  }
};

export const mapStateToProps = (state) => {
  const { isGameOn, drawnNumbers } = state;
  return {
    isGameOn,
    drawnNumbers
  }
};
