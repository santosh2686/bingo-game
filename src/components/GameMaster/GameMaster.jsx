import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {mapStateToProps, mapDispatchToProps} from './gameMaster.conf';

const GameMaster = ({
  drawNewNumber,
  drawnNumbers,
  startGame,
  stopGame,
  isGameOn,
  restartGame
}) => {
  return (
    <div className="game-master">
      <button disabled={isGameOn} onClick={startGame}>Start Game</button>
      <button disabled={!isGameOn} onClick={stopGame}>Pause Game</button>
      <button disabled={!isGameOn} onClick={restartGame}>Restart Game</button>
      <button disabled={drawnNumbers.length === 99} onClick={drawNewNumber}>Draw New Number</button>
    </div>
  )
};

GameMaster.prototypes = {
  drawnNumbers: PropTypes.array,
  drawNewNumber: PropTypes.func,
  startGame: PropTypes.func,
  stopGame: PropTypes.func,
  isGameOn: PropTypes.bool
};

GameMaster.defaultProps = {
  isGameOn: false,
  drawnNumbers: []
};

export default connect(mapStateToProps, mapDispatchToProps)(GameMaster);
