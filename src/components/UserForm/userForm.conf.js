import {loadPlayerInitialData} from '../../actions';
export const mapDispatchToProps = (dispatch, ownProps, state) => {
  return {
    submitAction: (name) => {
      dispatch(loadPlayerInitialData(name));
    }
  }
};

export const mapStateToProps = (state) => {
  return {}
};
