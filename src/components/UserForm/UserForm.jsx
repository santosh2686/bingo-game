import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {mapStateToProps, mapDispatchToProps} from './userForm.conf';

class UserForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: ''
    };

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleNameChange(event) {
    this.setState({name: event.target.value});
  }

  handleSubmit() {
    this.props.submitAction(this.state.name);
  }

  render() {
    return (
      <div className="user-detail">
        <div>
          <input id="name" type="text" value={this.state.name} onChange={this.handleNameChange} autoFocus placeholder="Enter Your Name"/>
        </div>
        <button type="button" disabled={!this.state.name} onClick={this.handleSubmit}>Submit</button>
      </div>
    );
  }
}

UserForm.prototypes = {
  name: PropTypes.string
};

export default connect(mapStateToProps, mapDispatchToProps)(UserForm);
