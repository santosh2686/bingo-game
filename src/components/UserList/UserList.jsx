import React from 'react';
import PropTypes from 'prop-types';

const UserList = ({ list }) => {
  const users = list.map((item) => {
    return <div key={item.id}>{item.name}</div>;
  });

  return (
    <div className="user-list">
      <div className="user-header">Online Players</div>
      {users}
    </div>
  )
};

UserList.prototypes = {
  list: PropTypes.array
};
UserList.defaultProps = {
  list: []
};

export default UserList;
