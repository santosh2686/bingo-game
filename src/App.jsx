import React from 'react';
import {Provider} from 'react-redux';
import {configureStore} from './configureStore';
import Container from './Container/Container.jsx';

const initialState = {
  drawnNumbers: [],
  cardDetail: [],
  numInCard: [],
  playerList: [],
  id: '',
  isGameOn: false,
  isActive: false,
  name: ''
};

const store = configureStore(initialState);

const App = () => (
  <Provider store={store}>
        <Container />
  </Provider>
);


export default App;