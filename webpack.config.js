const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: './src/index.js',
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, './build'),
    filename: 'bundle.js',
    publicPath: '/build/'
  },
  devServer: {
    port: process.env.PORT || 6060,
    host: '0.0.0.0',
    historyApiFallback: true,
    overlay: true,
    compress: true,
    proxy: {
      '/api/*': {
        target: 'ws://localhost:5050',
        ws: true,
      },
    }
  },
  stats: {
    colors: true,
    modules: true,
    assets: true,
    children: true,
    chunks: true,
    depth: true,
    entrypoints: true,
    providedExports: true,
    usedExports: true,
    timings: true,
    hash: true,
    maxModules: 0,
    errors: true,
    warnings: true
  },
  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel-loader'
    }, {
      test: /\.s?css$/,
      use: ['style-loader', 'css-loader', 'sass-loader']
    }]
  }
};
